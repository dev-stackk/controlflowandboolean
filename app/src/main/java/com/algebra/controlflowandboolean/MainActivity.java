package com.algebra.controlflowandboolean;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private CheckBox cbLight;
    private CheckBox cbWater;
    private CheckBox cbTelevision;
    private Button bTest;
    private EditText etInput;
    private TextView tvResult;

    private final static String PASSWORD = "abc123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        setupListeners();
    }

    private void setupListeners() {
        bTest.setOnClickListener(view -> {
            boolean water = cbWater.isChecked();
            boolean light = cbLight.isChecked();
            boolean television = cbTelevision.isChecked();
            //printResult(water);
            //printResult(!water);

            boolean negatedWater = !water;

            //printResult(negatedWater);

            boolean waterOrTelevision = water || television;
            printResult(waterOrTelevision);

            boolean waterAndTelevision = water && television;

            printResult(waterAndTelevision);

            boolean waterAndLightAndTv = water && light && television;

            printResult(waterAndLightAndTv);

            if (water) {
                printText("Water is running");
            }

            if (water && television) {
                printText("Water and TV is running");
            }

            if (light) {
                printText("Light is on, turn it off");
            } else {
                printText("Nice job turning off the light");
            }

            String messages = "";

            if (light) {
                messages = messages + "Light is on, turn it off\n";
            } else if (water) {
                messages = messages + "Water is on, turn it off\n";
            } else if (television) {
                messages = messages + "TV is on, turn it off\n";
            }

            printText(messages);

            messages = "";

            if (light) {
                messages = messages + "Light is on, turn it off\n";
            }
            if (water) {
                messages = messages + "Water is on, turn it off\n";
            }
            if (television) {
                messages = messages + "TV is on, turn it off\n";
            }

            printText(messages);
            String text = getText();

            messages = "";

            switch (text) {
                case "TV":
                    messages = messages + "Tv is on";
                    break;
                case "Light":
                    messages = messages + "Light is on";
                    break;
                case "PC":
                    messages = messages + "PC is on";
                    break;
                case "Water":
                    messages = messages + "Water is on";
                    break;
                default:
                    messages = messages + " All right";
            }

            printText(messages);

            messages = "";

            switch (text) {
                case "TV":
                    messages = messages + "Tv is on";
                case "Light":
                    messages = messages + "Light is on";
                case "PC":
                    messages = messages + "PC is on";
                case "Water":
                    messages = messages + "Water is on";
                default:
                    messages = messages + " All right";
            }

            String godisnjeDoba = getText();

            switch (godisnjeDoba) {
                case "Zima":
                    Toast.makeText(this, "Hladno", Toast.LENGTH_LONG).show();
                    break;
                case "Proljece":
                    Toast.makeText(this, "Toplije", Toast.LENGTH_LONG).show();
                    break;
                case "Ljeto":
                    Toast.makeText(this, "Vruce", Toast.LENGTH_LONG).show();
                    break;
                case "Jesen":
                    Toast.makeText(this, "Hladnije", Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(this, "Nepoznato godisnje doba", Toast.LENGTH_LONG).show();
            }

            printText(messages);

            printText("");
            boolean isPasswordOk = PASSWORD.equals(text);
            if (isPasswordOk) {
                Toast.makeText(this, "Successful login", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "You entered wrong password. Try again", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initWidgets() {
        cbWater = findViewById(R.id.cbWater);
        cbLight = findViewById(R.id.cbLight);
        cbTelevision = findViewById(R.id.cbTelevision);
        bTest = findViewById(R.id.bTest);
        etInput = findViewById(R.id.editText);
        tvResult = findViewById(R.id.tvResult);
    }

    private String getText() {
        return etInput.getText().toString();
    }

    private void printResult(boolean result) {
        tvResult.setText("Result is: " + result + "! \n" +
                "water: " + cbWater.isChecked() + "\n" +
                "light: " + cbLight.isChecked() + "\n" +
                "television: " + cbTelevision.isChecked() + "\n");
    }

    private void printText(String text) {
        tvResult.setText(text);
    }

}
